<?php
session_start();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

//Si c'est le joueure 1 alors on renvoie ceux du joueur 2

$req = $bdd->prepare('SELECT ACES_player1,TWOS_player1,THREES_player1,FOURS_player1,FIVES_player1,SIXES_player1, ACES_player2,TWOS_player2,THREES_player2,FOURS_player2,FIVES_player2,SIXES_player2 FROM score WHERE id_game=:id');
$req->execute(array(
  'id' => $_SESSION['game_number']));
  $result = $req->fetch(); //Récupération des infos
  $req->closeCursor();

  for ($i=0; $i < 11 ; $i++) {
    if ($result[i] < 1)
    {
      $result[i] = 0;
    }
  }

//Renvoie les scores
echo json_encode($result);
 ?>
