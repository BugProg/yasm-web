<?php
session_start ();
include ("config/config.php");

//Vérifie le code rentrer par l'utilisateur qui tente de rejoindre.

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}
?>

  <?php
  $_SESSION['game_number'] = $_POST['game_number'];
  $_SESSION['pseudo_player2'] = $_POST['pseudo_player2'];

  //Vérification que la partie éxiste bien
  //Récupération du pseudo du player1
  $req = $bdd->prepare('SELECT player1 FROM games WHERE id=:id');
  $req->execute(array(
    'id' => $_POST['game_number']));
    $result = $req->fetch(); //Récupération des infos
    $req->closeCursor();


  if ($result['player1'] != '') // Si player1 éxiste alors on rentre dans la partie
  {
    //Enregistrement dans la base de donnée.
    $req = $bdd->prepare('UPDATE games SET player2=:player2 WHERE id=:id');
    $req->execute(array(
      'id' => $_SESSION['game_number'],
      'player2' => $_POST['pseudo_player2'],
    ));
    header ('Location: /lobby.php');
    exit();
  }
  else
  {
    header ('Location: /index.html');
    exit();
  }
  ?>
