<?php
session_start ();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}
// ...
// on se connecte à notre base de données
$name = $_POST['user_name'];




// on récupère les 10 derniers messages postés

$requete = $bdd->prepare('SELECT * FROM messages WHERE id_game=:id_game ORDER BY id DESC LIMIT 0,15');
$requete->execute(array(
  'id_game' => $_SESSION['game_number']
));

  $messages = null;

  // on inscrit tous les nouveaux messages dans une variable
  while($donnees = $requete->fetch()){
    if ($donnees['pseudo_sender'] == $_SESSION['pseudo_player1'])
    $messages .= "<p style=\"color:green;\" > " . $donnees['pseudo_sender'] . " : " . $donnees['message'] . "</p>";
    else
    $messages .= "<p> " . $donnees['pseudo_sender'] . " : " . $donnees['message'] . "</p>";
  }
  $requete->closeCursor();

  echo $messages; // enfin, on retourne les messages à notre script JS


  ?>
