<?php
session_start();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}


    if(!empty($_POST['message'])){ // si les variables ne sont pas vides

      if ($_SESSION['status_player'] == 1)
      {
        $pseudo = $_SESSION['pseudo_player1'];
      }
        else
        {
        $pseudo = $_SESSION['pseudo_player2'];
      }

        $message = $_POST['message'];

        // puis on entre les données en base de données :
        $insertion = $bdd->prepare('INSERT INTO messages ( id_game, pseudo_sender, message) VALUES(:id_game, :pseudo_sender, :message)');
        $insertion->execute(array(
            'id_game' => $_SESSION['game_number'],
            'pseudo_sender' => $pseudo,
            'message' => $message
        ));
    }
?>
