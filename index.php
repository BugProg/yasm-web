<?php
session_start();
$_SESSION['pseudo_player1'] = '';
$_SESSION['pseudo_player2'] = '';
$_SESSION['game_number'] = '';

session_destroy();
 ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Yasm</title>
  <link rel="icon" type="image/png" href="img/favicon.png"/>

  <link rel="stylesheet" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
  <script>
  //initialisation
  $(function(){
    $("#div_home").animate({left: '40%'});
    $("#div_join").hide(100);
    $("#div_join").animate({opacity: '0'});
    $("#div_create_game").hide(100);
    $("#div_create_game").animate({opacity: '0'});
  });
  //Quand le boutton "rejoindre est cliqué"
  $(document).ready(function(){
    $("#button_join_game").click(function(){
      $("#div_home").animate({left: '80%', opacity: '0.3'});
      $("#div_home").hide(0);
      $("#div_join").show(0);
      $("#div_join").animate({left: '40%', opacity: '1'});
    });
  });

  //Quand le boutton "home"
  $(document).ready(function(){
    // $("#button_home").click(function(){
    $(".button_retour").click(function(){
      $("#div_home").show(0);
      $("#div_home").animate({left: '40%',opacity: '1'});
      $("#div_join").hide(100);
      $("#div_join").animate({opacity: '0'});
      $("#div_create_game").hide(100);
      $("#div_create_game").animate({opacity: '0'});
    });
  });

  //Quand le boutton "Create game"
  $(document).ready(function(){
    $("#button_create_new_game").click(function(){
      $("#div_create_game").show(0);
      $("#div_create_game").animate({left: '40%',opacity: '1'});
      $("#div_home").hide(100);
      $("#div_home").animate({opacity: '0'});
    });
  });
  </script>

  <div id="div_home" class="champ">
    <p>Yasm</p>
    <button  type="button" id="button_create_new_game" class="button" name="button">Créer une partie.</button>
    <br>
    <button type="button" id="button_join_game" class="button" name="button">Rejoindre une partie</button>
  </div>

<!-- Rejoindre une partie -->
<div id="div_join" class="champ">
  <p>Yasm</p>
  <form action="check_code.php" method="post">
    <input type="text" name="pseudo_player2" required placeholder="Your pseudo">
    <br>
    <input type="tel" name="game_number" required placeholder="Your game number">
    <br>
    <button type="submit" class="button" name="button">Next</button>
  </form>
  <br>
  <button class="button_retour button" type="button" id="button_home" name="button">Home</button>
</div>

  <!-- Création d'une nouvelle partie -->
  <div id="div_create_game"  class="champ">
    <p>Yasm</p>
    <form action="create_games.php" method="post">
      <!-- Définition d'un pseudo pour l'utilisateur -->
      <input type="text" name="pseudo_player1" required placeholder="Your pseudo">
      <br>
      <button type="submit" class="button" name="button">Next</button>
    </form>

    <button class="button_retour button" type="button" id="button_home" name="button">Home</button>
  </div>
</body>
</html>
