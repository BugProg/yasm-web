<?php
session_start();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Yasm</title>
  <link rel="icon" type="image/png" href="img/favicon.png"/>

  <link rel="stylesheet" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
  <?php

  //Récupération du pseudo du player1 et player2
  $req = $bdd->prepare('SELECT player1,player2,start FROM games WHERE id=:id');
  $req->execute(array(
    'id' => $_SESSION['game_number']));
    $result = $req->fetch(); //Récupération des infos
    $req->closeCursor();

    if ($_SESSION['status_player'] == 1) //si c'est le joueur 1
    {
      echo '<p>Bonjour, '.$_SESSION['pseudo_player1'].'</p>';
      echo '<p>Votre code est :'.$_SESSION['game_number'].'</p>';

      //Si l'utilisateur 2 n'est pas arrivé
      if ($result['player2'] == '')
      {
        echo 'En attente du joueur...';
        echo "<br>";
        echo $result['player2'];
      }
      else
      {
        $_SESSION['pseudo_player2'] = $result['player2'];
        echo '<p>Le joueur '.$result['player2'].' est connecté.</p>';
        echo '<button onclick="location.href = \'games.php\';" class="button_retour button" >Go !!</button>';
      }
    }
    else
    {
      $_SESSION['pseudo_player1'] = $result['player1'];
      $_SESSION['status_player'] = 2;
      echo '<p>Bonjour, '.$_SESSION['pseudo_player2'].'';
      echo '<p>'.$result['player1'].' sera votre adversaire</p>';
      echo 'En attente que '.$result['player1'].' lance la partie';

      if ($result['start'] == 1)
      {
        header ('Location: games.php');
        exit();
      }
    }
    ?>

    <!-- Rafraichie la pages toutes les secondes -->
    <script>
    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
      time = new Date().getTime();
    });

    function refresh() {
      if(new Date().getTime() - time >= 2000)
      window.location.reload(true);
      else
      setTimeout(refresh, 1000);
    }

    setTimeout(refresh, 1000);

    // Vérification que la partie est bien créer
    var code_game = "<?php echo $_SESSION['game_number']; ?>"
    if (code_game == '')
    {
      var r = confirm("La partie à été intérompue.\n Si ce message persiste penser à autoriser les cookies");
      if (r == true) {
        window.location = "http://yasm.home-cloud.fr";
      } else {
        window.location = "http://yasm.home-cloud.fr";
      }
    }
    </script>
  </body>
  </html>
