<?php
// Traite le résultat des différents
// echo $_POST['die']['1'];
// echo $_POST['die']['2'];
// echo $_POST['die']['3'];
// echo $_POST['die']['4'];
// echo $_POST['die']['5'];

//test des possibilités

//init vars
$number_of_1 = 0;
$number_of_2 = 0;
$number_of_3 = 0;
$number_of_4 = 0;
$number_of_5 = 0;
$number_of_6 = 0;

$aces_score = 0;
$twos_score = 0;
$three_score = 0;
$fours_score = 0;
$fives_score = 0;
$sixes_score = 0;

$trips_score = 0;
$quads_score = 0;
$full_score = 0;
$small_score = 0;
$large_score = 0;
$yazi_score = 0;

//Compte le nombre de repétition
for ($i=1; $i < 7; $i++) {
  if ($_POST['die'][$i] == 1) {
    $number_of_1 ++;
  }

  if ($_POST['die'][$i] == 2) {
    $number_of_2 ++;
  }

  if ($_POST['die'][$i] == 3) {
    $number_of_3 ++;
  }

  if ($_POST['die'][$i] == 4) {
    $number_of_4 ++;
  }

  if ($_POST['die'][$i] == 5) {
    $number_of_5 ++;
  }

  if ($_POST['die'][$i] == 6) {
    $number_of_6 ++;
  }
}

// **** ACES ****
if ($number_of_1 >= 3)
{
  $aces_score = 5;
}
elseif ($number_of_2 >= 3 or $number_of_3 >= 3 or $number_of_4 >= 3 or $number_of_5 >= 3 or $number_of_6 >= 3)
{
  $aces_score = 2;
}
// echo $aces_score;

// **** TWOS ****
if ($number_of_2 >= 3)
{
  $twos_score = 6;
}
elseif ($number_of_1 >= 3 or $number_of_3 >= 3 or $number_of_4 >= 3 or $number_of_5 >= 3 or $number_of_6 >= 3)
{
  $twos_score = 2;
}
// echo $twos_score;

// **** THREE ****
if ($number_of_3 >= 3)
{
  $three_score = 8;
}
elseif ($number_of_1 >= 3 or $number_of_2 >= 3 or $number_of_4 >= 3 or $number_of_5 >= 3 or $number_of_6 >= 3)
{
  $three_score = 2;
}
// echo $three_score;


// **** FOURS ****
if ($number_of_4 >= 3)
{
  $fours_score = 10;
}
elseif ($number_of_1 >= 3 or $number_of_2 >= 3 or $number_of_3 >= 3 or $number_of_5 >= 3 or $number_of_6 >= 3)
{
  $fours_score = 2;
}
// echo $fours_score;


// **** FIVES ****
if ($number_of_5 >= 3)
{
  $fives_score = 12;
}
elseif ($number_of_1 >= 3 or $number_of_2 >= 3 or $number_of_3 >= 3 or $number_of_4 >= 3 or $number_of_6 >= 3)
{
  $fives_score = 2;
}
// echo $fives_score;


// **** SIXES ****
if ($number_of_6 >= 3)
{
  $sixes_score = 15;
}
elseif ($number_of_1 >= 3 or $number_of_2 >= 3 or $number_of_3 >= 3 or $number_of_4 >= 3 or $number_of_5 >= 3)
{
  $sixes_score = 2;
}
// echo $sixes_score;

// **** TRIPS ****
if ($number_of_6 >= 3)
{
  $trips_score = 30;
}

// **** QUADS ****
if ($number_of_6 >= 4)
{
  $quads_score = 40;
}

// **** FULL ****
if ($number_of_6 >= 3 and $number_of_5 >= 2)
{
  $full_score = 60;
}

// **** SMALL ****
if ($number_of_1 >= 1 and $number_of_2 >= 1 and $number_of_3 >= 1 and $number_of_4 >= 1)
{
  $small_score = 40;
}

// **** LARGE ****
if ($number_of_1 >= 1 and $number_of_2 >= 1 and $number_of_3 >= 1 and $number_of_4 >= 1 and $number_of_5 >= 1)
{
  $large_score = 40;
}

// **** YAZI ****
if ($number_of_6 == 5)
{
  $yazi_score = 100;
}


//Send scores
$scores = array($aces_score, $twos_score, $three_score, $fours_score, $fives_score, $sixes_score, $trips_score, $quads_score, $full_score, $small_score, $large_score, $yazi_score);

echo json_encode($scores);

// echo ''.$number_of_1.''.$number_of_2.''.$number_of_3.' '.$number_of_4.' '.$number_of_5.'';
?>
