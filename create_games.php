<?php
session_start(); //Ouverture de session
include ("config/config.php");
// Salle d'attente des joueurs.

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}


//Si le pseudo est différent on crée une nouvelle partie
if ($_SESSION['pseudo_player1'] != $_POST['pseudo_player1'])
{
  $_SESSION['game_number'] = '';
}

$_SESSION['pseudo_player1'] = $_POST['pseudo_player1'];

//Création du jeux
if ($_SESSION['game_number'] == '')
{
  $_SESSION ['status_player'] = 1; //L'utilisateur est le joueur numéro 1
  $_SESSION ['game_number'] = rand(1000, 9999); //Nombre aléatoire définissant le code unique

  //Enregistrement dans la base de donnée de l'id et du pseudo.
  $req = $bdd->prepare('INSERT INTO games (id, player1) VALUES(:id, :player1)');
  $req->execute(array(
    'id' => $_SESSION['game_number'],
    'player1' => $_SESSION['pseudo_player1'],
  ));

  //CRéation de la table des scores
  $req = $bdd->prepare('INSERT INTO score (id_game) VALUES(:id)');
  $req->execute(array(
    'id' => $_SESSION['game_number']
  ));
}

//Renvoie vers la salle d'attente.
header('Location: lobby.php');
exit ();
?>
