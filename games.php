<?php
session_start();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}


//On récupère les infos depuis le serveur
$req = $bdd->prepare('SELECT player1,player2 FROM games WHERE id = :id');
$req->execute(array(
  'id' => $_SESSION['game_number']));
  $result = $req->fetch(); //Récupération des infos
  $req->closeCursor();


  //On lance la partie pour que le joueur2 puisse rejoindre automatiquement
  $req = $bdd->prepare('UPDATE games SET start=:start WHERE id=:id');
  $req->execute(array(
    'start' => 1,
    'id' => $_SESSION['game_number'],
  ));
  ?>


  <!DOCTYPE html>
  <html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Yasm</title>
    <link rel="icon" type="image/png" href="img/favicon.png"/>
    <link rel="stylesheet" href="css/games.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>




    <table style="border-spacing: 0px;">
      <tr>
        <th>
          <div class="messages" style="text-align:left; border:2px solid black;" id="messages">
            <!-- les messages du tchat -->
          </div>
        </th>
      </tr>

      <tr>
        <td>
          <!-- method="POST" action="traitement.php" -->
          <form id="form" >
            <input style="width: 40vh; border-top: 3px solid black;" type="text" placeholder="Votre message" name="message" id="message"/> <br>
            <input style="width: 40vh;" class="button" type="submit" id="submit" />
          </form>
        </td>
      </tr>
    </table>





    <!-- Mise en place des scores -->
    <ul style="left: 20px; top: 40vh; position: absolute; list-style-type: none;">
      <li> <h1>Scores :</h1> </li>
      <li> <h4><?php echo ''.$_SESSION['pseudo_player1'].' :'; ?></h4> <h3 id="score_player1"></h3> </li>
      <li> <h4><?php echo ''.$_SESSION['pseudo_player2'].' :'; ?></h4> <h3 id="score_player2"></h3> </li>
    </ul>



    <div class="die_class">


      <!-- Mise en place des dés et du boutton de lancé -->
      <table>
        <tr>
          <script>
          var score = Array (0,0,0,0,0,0,0,0,0,0);


          //initialisation des dés comme actif
          // 1 :Dé actif ; 0 : Dé non actif
          var statusDie = Array (1,1,1,1,1,1);
          //Une valeur ici en plus pour ne pas prendre l'index 0
          </script>

          <!-- dé 1 -->
          <th>
            <div>
              <button onclick="statusDie[1] =! statusDie[1]" type="button" name="button button_die" style="background-color: transparent; border-color: transparent;" ><img id="die1" class="die_img" src="img/1.png"></button>
            </div>
          </th>

          <!-- dé 2 -->
          <th>
            <div>
              <button onclick="statusDie[2] =! statusDie[2]" type="button" name="button" style="background-color: transparent; border-color: transparent;" ><img id="die2" class="die_img" src="img/2.png"></button>
            </div>
          </th>

          <!-- dé 3 -->
          <th>
            <div>
              <button onclick="statusDie[3] =! statusDie[3]" type="button" name="button" style="background-color: transparent; border-color: transparent;" ><img id="die3" class="die_img" src="img/3.png"></button>
            </div>
          </th>

          <!-- dé 4 -->
          <th>
            <div>
              <button onclick="statusDie[4] =! statusDie[4]" type="button" name="button" style="background-color: transparent; border-color: transparent;" ><img id="die4" class="die_img" src="img/4.png"></button>
            </div>
          </th>

          <!-- dé 5 -->
          <th>
            <div>
              <button onclick="statusDie[5] =! statusDie[5]" type="button" name="button" style="background-color: transparent; border-color: transparent;" ><img id="die5" class="die_img" src="img/5.png"></button>
            </div>
          </th>

        </tr>
      </table>
      <footer>
        <button onclick="random_die()" style="margin-left: 50px;" type="submit" id="launch_button" class="button" name="button">Lancer !!</button>
      </footer>
    </div>

    <div style="top: 0; right: 0;position:absolute;">

      <table style="border: 2px solid black; border-radius: 10px;">
        <!-- Ligne 1 -->
        <tr>
          <td></td>
          <td>Player 1</td>
          <td>Player 2</td>
          <td></td>
          <td>Player 1</td>
          <td>Player 2</td>
        </tr>

        <!-- Ligne 2 -->
        <tr>
          <th>ACES <br> <img class="die_table" src="img/1.png"> <img class="die_table" src="img/1.png"> <img class="die_table" src="img/1.png"></th>
          <th> <button id="ACES_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="ACES_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>TRIPS <br> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"></th>
          <th> <button id="TRIPS_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="TRIPS_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 3 -->
        <tr>
          <th>TWOS <br> <img class="die_table" src="img/2.png"> <img class="die_table" src="img/2.png"> <img class="die_table" src="img/2.png"></th>
          <th> <button id="TWOS_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="TWOS_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>QUADS <br>  <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"></th>
          <th> <button id="QUADS_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="QUADS_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 4 -->
        <tr>
          <th>THREES <br> <img class="die_table" src="img/3.png"> <img class="die_table" src="img/3.png"> <img class="die_table" src="img/3.png"></th>
          <th> <button id="THREES_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="THREES_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>FULL <br>  <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/5.png"> <img class="die_table" src="img/5.png"></th>
          <th> <button id="FULL_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="FULL_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 5 -->
        <tr>
          <th>FOURS <br> <img class="die_table" src="img/4.png"> <img class="die_table" src="img/4.png"> <img class="die_table" src="img/4.png"></th>
          <th> <button id="FOURS_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="FOURS_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>SMALL <br>  <img class="die_table" src="img/1.png"> <img class="die_table" src="img/2.png"> <img class="die_table" src="img/3.png"> <img class="die_table" src="img/4.png"></th>
          <th> <button id="SMALL_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="SMALL_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 6 -->
        <tr>
          <th>FIVES <br> <img class="die_table" src="img/5.png"> <img class="die_table" src="img/5.png"> <img class="die_table" src="img/5.png"></th>
          <th> <button id="FIVES_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="FIVES_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>LARGE <br>  <img class="die_table" src="img/1.png"> <img class="die_table" src="img/2.png"> <img class="die_table" src="img/3.png"> <img class="die_table" src="img/4.png"> <img class="die_table" src="img/5.png"> </th>
          <th> <button id="LARGE_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="LARGE_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 7 -->
        <tr>
          <th>SIXES <br> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"></th>
          <th> <button id="SIXES_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="SIXES_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th>YAZI <br>  <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"> <img class="die_table" src="img/6.png"></th>
          <th> <button id="YAZI_player1" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
          <th> <button id="YAZI_player2" onClick="game_center(this.id)" type="button" name="button" style="/*Ne pas supprimer*/ border:none; background:transparent;"></button> </th>
        </tr>

        <!-- Ligne 8 -->
        <tr>
          <th>Bonus <br> if subtotal > 62</th>
          <th></th>
          <th></th>
          <th>CHANCE <br> </th>
          <th></th>
          <th></th>
        </tr>
      </table>
    </div>

    <style media="screen">

    .die_table
    {
      height: 25px;
      width: auto;
    }
    table, th
    {
      text-align: left;
    }
    tr:nth-child(even) {background-color: #f2f2f2;}

    table
    {
      border-collapse: collapse;
    }
    </style>

  </body>
  </html>

  <script>

  //Envoie automatique du message sans rafraichissement
  $(document).ready(function(){

    $("#submit").click(function(e){

      e.preventDefault();

      $.post(
        'save_message.php',
        {
          message : $("#message").val() //Envoie du message de l'utilisateur
        },

        'text'
      );
      document.getElementById("form").reset();

    });
  });

</script>


<!-- //Récupération des messages depuis le serveur. -->
<script>
function loadlink(){//Récupération des messages depuis le serveur.
  $('#messages').load('refresh_message.php',function () {
    // $(this).unwrap();
  });
}

loadlink(); // This will run on page load
setInterval(function(){
  loadlink() // this will run after every 5 seconds
}, 1000);

</script>

<script>
//initialisation de la variables des scores temporaires
//Les scores temporaires sont ceux affichier l'or du lancement du dé mais pas validés
var tmp_scores = Array ();

//Récuperation du status de joueur (player1 ou player2)
var status_player = <?php echo $_SESSION['status_player'];?>;
console.log (status_player);

var die_number = Array (1,1,1,1,1,1); //Init du dé // ICI 6 valeurs pour ne pas prendre l'index 0
// Lance les dés
function random_die()
{

  for (var i = 1; i < 6; i++) {
    //Génération d'un nombre aléatoire; init du chemin des images en fonction du nombre obtenue ainsi que l'id
    var x = Math.trunc ((Math.random() - 0) * (7 - 1) / (1 - 0) + 1), path = "img/"+x+".png", id = "die"+i;


    if (statusDie[i] == 1) //Si le dé est activé
    {
      document.getElementById(id).src = path; //changement de l'image
      die_number[i] = x;
    }
  }

  //Envoie des valeurs des dés au fichier php.
  $.ajax({
    type: "POST",
    url: 'dies_process.php',
    data: {die: die_number},
    success: function(data){
      tmp_scores = JSON.parse(data);
    }
  });
  update_style ();
}

function update_style ()
{
  //Change le style des dé en fonction de son état
  for (var i = 1; i < 6; i++) {

    var id = "die"+i;

    if (statusDie[i] == 1) //Si le dé est activé
    document.getElementById(id).style = "background-color: transparent; border: 3px solid transparent;"; //changement du style

    else
    document.getElementById(id).style = "background-color: transparent;  border: 3px solid black; border-radius: 10px;";

  }
  setTimeout('update_style()',500); /* rappel après 0,5 secondes = 500 millisecondes */

  //Change les scores (Juste la previsualisation) Ne change rien dans la base de donnée

  if (score[0] == 0)//Si le score est à 0 alors l'utilisateur a déja validé le point
  document.getElementById("ACES_player"+status_player).innerHTML = tmp_scores[0];
  if (score[1] == 0)
  document.getElementById("TWOS_player"+status_player).innerHTML = tmp_scores[1];
  if (score[2] == 0)
  document.getElementById("THREES_player"+status_player).innerHTML = tmp_scores[2];
  if (score[3] == 0)
  document.getElementById("FOURS_player"+status_player).innerHTML = tmp_scores[3];
  if (score[4] == 0)
  document.getElementById("FIVES_player"+status_player).innerHTML = tmp_scores[4];
  if (score[5] == 0)
  document.getElementById("SIXES_player"+status_player).innerHTML = tmp_scores[5];


  document.getElementById("TRIPS_player"+status_player).innerHTML = tmp_scores[6];
  document.getElementById("QUADS_player"+status_player).innerHTML = tmp_scores[7];
  document.getElementById("FULL_player"+status_player).innerHTML = tmp_scores[8];
  document.getElementById("SMALL_player"+status_player).innerHTML = tmp_scores[9];
  document.getElementById("LARGE_player"+status_player).innerHTML = tmp_scores[10];
  document.getElementById("YAZI_player"+status_player).innerHTML = tmp_scores[11];

}
update_style();


//Change le sytle des boutons en fonction du joueur
document.getElementById("ACES_player"+status_player).style.border = "1px solid black";
document.getElementById("TWOS_player"+status_player).style.border = "1px solid black";
document.getElementById("THREES_player"+status_player).style.border = "1px solid black";
document.getElementById("FOURS_player"+status_player).style.border = "1px solid black";
document.getElementById("FIVES_player"+status_player).style.border = "1px solid black";
document.getElementById("SIXES_player"+status_player).style.border = "1px solid black";

document.getElementById("TRIPS_player"+status_player).style.border = "1px solid black";
document.getElementById("QUADS_player"+status_player).style.border = "1px solid black";
document.getElementById("FULL_player"+status_player).style.border = "1px solid black";
document.getElementById("SMALL_player"+status_player).style.border = "1px solid black";
document.getElementById("LARGE_player"+status_player).style.border = "1px solid black";
document.getElementById("YAZI_player"+status_player).style.border = "1px solid black";

function check_game ()
{
  // Vérification que la partie existe toujours
  var code_game = "<?php echo $_SESSION['game_number']; ?>"

  if (code_game == '')
  {
    var r = confirm("La partie à été intérompue.");
    if (r == true) {
      window.location = "http://yasm.home-cloud.fr";
    } else {
      window.location = "http://yasm.home-cloud.fr";
    }
  }
  setTimeout('check_game ()',3000);
}
check_game ();


//envoie les scores aux servers et récupère ceux de l'adversaire et gère qui joue qui ne joue pas
function game_center (button_id)
{
  //Récupération du score
  var score_to_send = 0;

  if ((button_id == "ACES_player1" || button_id == "ACES_player2") && score[0] == 0)
  {
    score[0] = tmp_scores[0];
    score_to_send = score[0];
  }
  if (button_id == "TWOS_player1" || button_id == "TWOS_player2" && score[1] == 0)
  {
    score[1] = tmp_scores[1];
    score_to_send = score[1];
  }
  if (button_id == "THREES_player1" || button_id == "THREES_player2" && score[2] == 0)
  {
    score[2] = tmp_scores[2];
    score_to_send = score[2];
  }
  if (button_id == "FOURS_player1" || button_id == "FOURS_player2" && score[3] == 0)
  {
    score[3] = tmp_scores[3];
    score_to_send = score[3];
  }
  if (button_id == "FIVES_player1" || button_id == "FIVES_player2" && score[4] == 0)
  {
    score[4] = tmp_scores[4];
    score_to_send = score[4];
  }
  if (button_id == "SIXES_player1" || button_id == "SIXES_player2" && score[5] == 0)
  {
    score[5] = tmp_scores[5];
    score_to_send = score[5];
  }
  if (button_id == "TRIPS_player1" || button_id == "TRIPS_player2" && score[6] == 0)
  {
    score[6] = tmp_scores[6];
    score_to_send = score[6];
  }
  if (button_id == "QUADS_player1" || button_id == "QUADS_player2" && score[7] == 0)
  {
    score[7] = tmp_scores[7];
    score_to_send = score[7];
  }
  if (button_id == "FULL_player1" || button_id == "FULL_player2" && score[8] == 0)
  {
    score[8] = tmp_scores[8];
    score_to_send = score[8];
  }
  if (button_id == "SMALL_player1" || button_id == "SMALL_player2" && score[9] == 0)
  {
    score[9] = tmp_scores[9];
    score_to_send = score[9];
  }
  if (button_id == "LARGE_player1" || button_id == "LARGE_player2" && score[10] == 0)
  {
    score[10] = tmp_scores[10];
    score_to_send = score[10];
  }
  if (button_id == "YAZI_player1" || button_id == "YAZI_player2" && score[11] == 0)
  {
    score[11] = tmp_scores[11];
    score_to_send = score[11];
  }

  //Si score_to_send = 0 alors l'utilisateur a déja valider le point et il n'est pas renvoyé
  if (score_to_send != 0)
  {
    $.ajax({
      type: "POST",
      url: 'game_center.php',
      data: {id: button_id, score: score_to_send},
      success: function(data){
        document.getElementById(button_id).style.border = "none";
      }
    });
  }

  //Récupération des scores
  var scores_opponent = Array ();
  $.ajax({
    type: "POST",
    url: 'get_score.php',
    data: {player: status_player},
    success: function(data){
      scores_opponent = JSON.parse(data);

      //Mise en place des score depuis la base de donnée
      //Player 1
      document.getElementById("ACES_player1").innerHTML = scores_opponent[0];
      document.getElementById("TWOS_player1").innerHTML = scores_opponent[1];
      document.getElementById("THREES_player1").innerHTML = scores_opponent[2];
      document.getElementById("FOURS_player1").innerHTML = scores_opponent[3];
      document.getElementById("FIVES_player1").innerHTML = scores_opponent[4];
      document.getElementById("SIXES_player1").innerHTML = scores_opponent[5];

      //Player2
      document.getElementById("ACES_player2").innerHTML = scores_opponent[6];
      document.getElementById("TWOS_player2").innerHTML = scores_opponent[7];
      document.getElementById("THREES_player2").innerHTML = scores_opponent[8];
      document.getElementById("FOURS_player2").innerHTML = scores_opponent[9];
      document.getElementById("FIVES_player2").innerHTML = scores_opponent[10];
      document.getElementById("SIXES_player2").innerHTML = scores_opponent[11];

      var score_player1 = 0;
      for (var i = 0; i < 6; i++) {
        if (scores_opponent[i] > 0)
        score_player1 += scores_opponent[i];
      }
      document.getElementById("score_player1").innerHTML = score_player1;

var score_player2 = 0;
      for (var i = 6; i < 12; i++) {
        if (scores_opponent[i] > 0)
        score_player2 += scores_opponent[i];
      }
      document.getElementById("score_player2").innerHTML = score_player2;
    }
  });



  setTimeout('game_center (null)',5000);
}
game_center (null);
</script>
