<?php
session_start();
include ("config/config.php");

try // tentative de connection à la base de données
{
  $bdd = new PDO('mysql:host='.$db_host.';dbname=yasm;charset=utf8', 'yasm', $db_password);
}
catch (Exception $e) // en cas d'echec on affiche les erreurs
{
  die('Erreur : ' . $e->getMessage());
}

if ($_POST['id'] == 'ACES_player1')
{
  $req = $bdd->prepare('UPDATE score SET ACES_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'ACES_player2')
{
  $req = $bdd->prepare('UPDATE score SET ACES_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}


if ($_POST['id'] == 'TWOS_player1')
{
  $req = $bdd->prepare('UPDATE score SET TWOS_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'TWOS_player2')
{
  $req = $bdd->prepare('UPDATE score SET TWOS_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}


if ($_POST['id'] == 'THREES_player1')
{
  $req = $bdd->prepare('UPDATE score SET THREES_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'THREES_player2')
{
  $req = $bdd->prepare('UPDATE score SET THREES_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}


if ($_POST['id'] == 'FOURS_player1')
{
  $req = $bdd->prepare('UPDATE score SET FOURS_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'FOURS_player2')
{
  $req = $bdd->prepare('UPDATE score SET FOURS_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}


if ($_POST['id'] == 'FIVES_player1')
{
  $req = $bdd->prepare('UPDATE score SET FIVES_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'FIVES_player2')
{
  $req = $bdd->prepare('UPDATE score SET FIVES_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}


if ($_POST['id'] == 'SIXES_player1')
{
  $req = $bdd->prepare('UPDATE score SET SIXES_player1=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}
if ($_POST['id'] == 'SIXES_player2')
{
  $req = $bdd->prepare('UPDATE score SET SIXES_player2=:score WHERE id_game=:game_number');
  $req->execute(array(
    'score' => $_POST['score'],
    'game_number' => $_SESSION['game_number']
  ));
}



echo $_POST['id'];
echo $_POST['score'];
echo $_SESSION['game_number'];
?>
